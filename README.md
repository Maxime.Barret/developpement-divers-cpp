**Développement divers en C++**

**_Abstract_**

In this repository, you will find general C++ codes I wrote in order to experiment.
- Derivation implements classes that represent classic math expressions (like 1+2 or (x+6)/y) and try to produce the expression of the differentiation associated.

- Lists is a cpp file that manipulates forward_lists, maps and vectors.

- Nombre implements a class that represent integers with N digit as a linked container.


**_Résumé_**

Dans ce répertoire se trouvent des codes C++ généraux que j'ai écrit pour m'entrainer.
- Dérivation implemente des classes représentant des expressions mathématiques classiques (comme 1+2 ou (x+6)/y) et produit l'expression de la dérivation associée.

- Lists est un fichier cpp qui manipule des forward_lists, des maps et des vectors.

- Nombre implémente une classe représentant des entiers à N chiffres comme un conteneur chaîné.

