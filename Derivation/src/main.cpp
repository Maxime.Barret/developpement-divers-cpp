#include <iostream>
#include <sstream>
#include "Expression.hpp"
#include "gtest/gtest.h"
 
 /*c++ -std=c++11 -o main main.cpp Expression.cpp -Igoogletest/googletest/include -Lgtest/lib -lgtest -lpthread -lgtest_main*/
 
TEST( TestDerivation, TestAffichageNombre )
{
    Nombre n( 2 );
    std::ostringstream os;
    os << n;
    EXPECT_EQ( os.str(), "2" );
}
 
TEST( TestDerivation, TestAffichageVariable )
{
    const Expression * e = new Variable( "x" );
    std::ostringstream os;
    os << *e;
    EXPECT_EQ(os.str(), "x" );
    delete e;
    EXPECT_EQ(Expression::compteur, 0 );  // Verification du compteur d'instance
}
 
TEST( TestDerivation, TestDerivationNombre )
{
    const Expression * e = new Nombre( -2 );
    const Expression * f = e->derive( "x" );
    ASSERT_TRUE( nullptr != f );
    std::ostringstream os;
    os << *f;
    EXPECT_EQ( os.str(), "0" );
    delete e;
    delete f;
    EXPECT_EQ(Expression::compteur, 0 );  // Verification du compteur d'instance
}
 
TEST( TestDerivation, TestDerivationVariable )
{
    const Expression * e = new Variable( "x" );
    const Expression * f = e->derive( "x" );
    ASSERT_TRUE( nullptr != f );
    std::ostringstream os1;
    os1 << *f;
    EXPECT_EQ( os1.str(), "1" );
    delete f;
 
    f = e->derive( "y" );
    ASSERT_TRUE( nullptr != f );
    std::ostringstream os2;
    os2 << *f;
    EXPECT_EQ( os2.str(), "0" );
    delete e;
    delete f;
    EXPECT_EQ(Expression::compteur, 0 );  // Verification du compteur d'instance
}
 
TEST( TestDerivation, TestAffichageAddition )
{
    const Expression * e = new Addition( new Variable( "x" ), new Nombre( -2 ));
    std::ostringstream os;
    os << *e;
    EXPECT_EQ( os.str(), "(x + -2)" );
    delete e;
    EXPECT_EQ(Expression::compteur, 0 );  // Verification du compteur d'instance
}
 
TEST( TestDerivation, TestDerivationAddition )
{
    const Expression * e = new Addition( new Variable( "x" ), new Nombre( -10 ));
    const Expression * f = e->derive( "x" );
    ASSERT_TRUE( nullptr != f );
    std::ostringstream os;
    os << *f;
    EXPECT_EQ( os.str(), "(1 + 0)" );
    delete e;
    delete f;
    EXPECT_EQ(Expression::compteur, 0 );  // Verification du compteur d'instance
}
 
TEST( TestDerivation, TestAffichageMultiplication )
{
    const Expression * e = new Multiplication( new Variable( "y" ), new Variable( "z" ));
    std::ostringstream os;
    os << *e;
    EXPECT_EQ( os.str(), "(y * z)" );
    delete e;
    EXPECT_EQ(Expression::compteur, 0 );  // Verification du compteur d'instance
}
 
TEST( TestDerivation, TestDerivationMultiplication )
{
    // This test expects (f * g)' = f' * g + f * g'
    const Expression * e = new Multiplication(
                         new Variable( "x" ),
                         new Multiplication( new Variable( "y" ), new Variable( "z" ))
                     );
    const Expression * f = e->derive( "x" );
    ASSERT_TRUE( nullptr != f );
    std::ostringstream os;
    os << *f;
    EXPECT_EQ( os.str(), "((1 * (y * z)) + (x * ((0 * z) + (y * 0))))" );
    delete e;
    delete f;
    EXPECT_EQ(Expression::compteur, 0 );  // Verification du compteur d'instance
}

TEST( TestDerivation, TestSimplificationAddition )
{
    // 2 + 6 = 8 (Addition de Nombre)
    const Expression * e1 = new Addition(
                        new Nombre(2),
                        new Nombre(6)
                        );
    const Expression * s1 = e1->simplification();
    ASSERT_TRUE(nullptr != s1);
    std::ostringstream os;
    os << *s1 << "_";   
    
    // 0 + x = x  (0 element neutre de Addition)
    const Expression * e2 = new Addition(
                        new Nombre(0),
                        new Variable("x")
                        );
    const Expression * s2 = e2->simplification();
    ASSERT_TRUE(nullptr != s2);
    os << *s2;    
    EXPECT_EQ( os.str(), "8_x" );
    delete e1;
    delete e2;
    delete s1;
    delete s2;
    EXPECT_EQ(Expression::compteur, 0 );  // Verification du compteur d'instance
}

TEST( TestDerivation, TestSimplificationMultiplication )
{
    // 2 * 6 = 12 (Multiplication de Nombre)
    const Expression * e1 = new Multiplication(
                        new Nombre(2),
                        new Nombre(6)
                        );
    const Expression * s1 = e1->simplification();
    ASSERT_TRUE(nullptr != s1);
    std::ostringstream os;
    os << *s1 << "_";   
    
    // 0 * x = 0  (0 element nul de la Multiplication)
    const Expression * e2 = new Multiplication(
                        new Nombre(0),
                        new Variable("x")
                        );
    const Expression * s2 = e2->simplification();
    ASSERT_TRUE(nullptr != s2);
    os << *s2 << "_";    

    // 1 * x = x  (1 element neutre de la Multiplication)
    const Expression * e3 = new Multiplication(
                        new Nombre(1),
                        new Variable("x")
                        );
    const Expression * s3 = e3->simplification();
    ASSERT_TRUE(nullptr != s3);
    os << *s3;    

    EXPECT_EQ( os.str(), "12_0_x" );
    delete e1;
    delete e2;
    delete e3;
    delete s1;
    delete s2;
    delete s3;
    EXPECT_EQ(Expression::compteur, 0 );  // Verification du compteur d'instance
}