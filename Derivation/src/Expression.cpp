#include <iostream>
#include "Expression.hpp"

/////// EXPRESSION ///////

int Expression::compteur = 0; // Compteur d'instance d'Expression

std::ostream & operator<<(std::ostream & os, const Expression & e)
{
	return e.print(os);
}

/////// NOMBRE ///////

Nombre::Nombre(const double n) : value_(n)
{
	std::cout << "compteur ++ " << *this << std::endl;
	compteur++;
}

std::ostream & Nombre::print(std::ostream & os) const
{
	return os << value_;
}

Nombre * Nombre::derive(std::string var) const
{
	return new Nombre(0);
}

Nombre * Nombre::clone() const
{
	return new Nombre(value_);
}

double Nombre::getValue() const
{
	return value_;
}

Nombre * Nombre::simplification() const 
{ 
	return this->clone(); 
}


/////// VARIABLE ///////

Variable::Variable(const std::string & v) : name_(v)
{
	std::cout << "compteur ++ " << *this << std::endl;
	compteur++;
}

std::ostream & Variable::print(std::ostream & os) const
{
	return os << name_;
}

Nombre * Variable::derive(std::string var) const
{
	return new Nombre(var == name_ ? 1 : 0);
}

Variable * Variable::clone() const
{
	return new Variable(name_);
}

std::string Variable::getName() const
{
	return name_;
}

Variable * Variable::simplification() const
{ 
	return this->clone(); 
}
/////// OPERATION ///////

Operation::~Operation()
{
	std::cout << "Operation -- " << op1_ << std::endl;
	delete op1_;
	std::cout << "Operation -- " << op2_ << std::endl;
	delete op2_;
}

/////// ADDITION ///////  

Addition::Addition(const Expression * o1, const Expression * o2)
{
	op1_ = o1;
	op2_ = o2;
	std::cout << "compteur ++ " << *this << std::endl;
	compteur++;
}

std::ostream & Addition::print(std::ostream &os) const
{
	return os << "(" << *op1_ << " + " << *op2_ << ")";
}

Addition * Addition::clone() const
{
	return new Addition(op1_->clone(), op2_->clone());
}

Addition * Addition::derive(std::string var) const
{
	return new Addition(op1_->derive(var), op2_->derive(var));
}

Expression * Addition::simplification() const
{
	const Nombre * o1 = dynamic_cast<const Nombre*>(op1_);
	const Nombre * o2 = dynamic_cast<const Nombre*>(op2_);
	if (o1 && !o2)
	{
		if (o1->getValue() == 0)
			return op2_->simplification();
	}
	else if (o2 && !o1)
	{
		if (o2->getValue() == 0)
			return op1_->simplification();

	}
	else if (o1 && o2)
		return new Nombre(o1->getValue() + o2->getValue());
	else
		return new Addition(op1_->simplification(), op2_->simplification());
		
}

/////// MULTIPLICATION ///////  

Multiplication::Multiplication(const Expression* o1, const Expression* o2)
{
	op1_ = o1;
	op2_ = o2;
	std::cout << "compteur ++ " << *this << std::endl;
	compteur++;
}

std::ostream & Multiplication::print(std::ostream &os) const
{
	return os << "(" << *op1_ << " * " << *op2_ << ")";
}

Multiplication * Multiplication::clone() const
{
	return new Multiplication(op1_->clone(), op2_->clone());
}

Addition * Multiplication::derive(std::string var) const
{
	return new Addition(new Multiplication(op1_->derive(var), op2_->clone()), 
						new Multiplication(op1_->clone(), op2_->derive(var)));
}

Expression * Multiplication::simplification() const
{
	const Nombre * o1 = dynamic_cast<const Nombre*>(op1_);
	const Nombre * o2 = dynamic_cast<const Nombre*>(op2_);
	if (o1 && !o2)
	{
		if (o1->getValue() == 0)
			return new Nombre(0);
		else if (o1->getValue() == 1)
			return op2_->simplification();
	}
	else if (o2 && !o1)
	{
		if (o2->getValue() == 0)
			return new Nombre(0);
		else if (o2->getValue() == 1)
			return op1_->simplification();
	}
	else if (o1 && o2)
		return new Nombre(o1->getValue() * o2->getValue());
	else
		return new Multiplication(op1_->simplification(), op2_->simplification());
}