#include <iostream>
#include <string>

class Expression
{
public:
	static int compteur;  // compteur d'instance d'Expression

	virtual ~Expression() { std::cout << "compteur -- " << std::endl; compteur--; }

	// print :
	//    Argument : os -> flux de sorti
	//    Retourne le flux modifié : return os << [...];
	virtual std::ostream & print(std::ostream & os) const = 0;

	// derive
	//    Argument : var -> chaine de caractères
	//    Retourne un pointeur sur une nouvelle instance d'une sous classe d'Expression par allocation dynamique
	//    Ne modifie pas la classe courrante (const post fixé)
	virtual Expression * derive(std::string var) const = 0;

	// clone
	//    Argument : None
	//    Retourne un pointeur sur une nouvelle instance de copie de la classe par allocation dynamique
	//    Ne modifie pas la classe courrante (const post fixé)
	virtual Expression * clone() const = 0;

	// simplification
	//	  Argument : None
	//	  Simplifie l'expression : (0 * Variable/Nombre) = Nombre(0) ; (1 * Variable/Nombre) = Variable/Nombre(Nombre) ; (Nombre +/* Nombre) = Nombre ; (0 + Variable) = Variable
	virtual Expression * simplification() const = 0;

	// operator<<
	//    Implementation de l'écriture d'une Expression par la méthode de la bibliothèque standard
	friend std::ostream & operator<<(std::ostream & os, const Expression & e);
};

class Nombre : public Expression
{
public:
	// Constructeur :
	//    Argument : n -> nombre flottant
	//    Donne à l'objet la valeur n pour value_
	Nombre(const double n);

	// Destructuer par defaut
	~Nombre() = default;

	// print : 
	//    Implementation de la methode print de Expression adaptée à Nombre
	virtual std::ostream & print(std::ostream & os) const override;

	// derive :
	//    Implementation de la methode derive de Expression adaptée à Nombre
	virtual Nombre * derive(std::string var) const override;

	// clone :
	//    Implementation de la methode clone de Expression adaptée à Nombre
	virtual Nombre * clone() const override;

	// simplification
	//		Renvoie un clone du Nombre
	virtual Nombre * simplification() const override;

	// getter pour l'attribut value_
	//
	double getValue() const;

private:
	const double value_;

};

class Variable : public Expression
{
public:
	// Constructeur :
	//    Argument : v -> chaine de caractères
	//    Donne à l'objet la valeur v pour name_
	Variable(const std::string & v);

	// Destructuer par defaut
	~Variable() = default;

	// print : 
	//    Implementation de la methode print de Expression adaptée à Variable
	virtual std::ostream & print(std::ostream & os) const override;

	// derive :
	//    Implementation de la methode derive de Expression adaptée à Variable 
	virtual Nombre * derive(std::string var) const override;

	// clone :
	//    Implementation de la methode clone de Expression adaptée à Variable
	virtual Variable * clone() const override;

	// simplification
	//	Renvoie un clone de la Variable
	virtual Variable * simplification() const override;

	// getter pour l'attribut name_
	//
	std::string getName() const;

private:
	const std::string name_;
};

class Operation : public Expression
{
public:
	virtual ~Operation();

	// print :
	//    Argument : os -> flux de sorti
	//    Retourne le flux modifié : return os << [...];
	virtual std::ostream & print(std::ostream & os) const = 0;

	// derive
	//    Argument : var -> chaine de caractères
	//    Retourne un pointeur sur une nouvelle instance d'une sous classe d'Expression par allocation dynamique
	//    Ne modifie pas la classe courrante (const post fixé)
	virtual Expression * derive(std::string var) const = 0;

	// clone
	//    Argument : None
	//    Retourne un pointeur sur une nouvelle instance de copie de la classe par allocation dynamique
	//    Ne modifie pas la classe courrante (const post fixé)
	virtual Expression * clone() const = 0;

	// simplification
	//	  Renvoie une Expression simplifiee (cf simplification classe Expression)
	virtual Expression * simplification() const = 0;

protected:
	const Expression * op1_;
	const Expression * op2_;
};

class Addition : public Operation
{
public:
	// Constructeur : 
	//    Argument : o1, o2 -> Expression
	//    Effectue la copie des arguments d'entrés car on ne gère pas le partage d'Expression
	Addition(const Expression * o1, const Expression * o2);

	// Destructuer par default
	~Addition() = default;

	// print :
	//    Implementation de la methode print de Expression adaptée à Addition
	virtual std::ostream & print(std::ostream & os) const override;

	// derive
	//    Implementation de la methode derive de Expression adaptée à Addition
	virtual Addition * derive(std::string var) const override;

	// clone
	//    Implementation de la methode clone de Expression adaptée à Addition
	virtual Addition * clone() const override;

	// simplification
	//	(cf simplifaction Expression)
	virtual Expression * simplification() const override;
};

class Multiplication : public Operation
{
public:
	// Constructeur : 
	//    Argument : o1, o2 -> Expression
	//    Effectue la copie des arguments d'entrés car on ne gère pas le partage d'Expression
	Multiplication(const Expression * o1, const Expression * o2);

	// Destructuer par default
	~Multiplication() = default;

	// print :
	//    Implementation de la methode print de Expression adaptée à Multiplication
	virtual std::ostream & print(std::ostream & os) const override;

	// derive
	//    Implementation de la methode derive de Expression adaptée à Multiplication
	virtual Addition * derive(std::string var) const override;

	// clone
	//    Implementation de la methode clone de Expression adaptée à Multiplication
	virtual Multiplication * clone() const override;

	// simplification
	//	(cf simplifaction Expression)
	virtual Expression * simplification() const override;
};