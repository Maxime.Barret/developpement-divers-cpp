#include <iostream>
#include <utility>
#include <sstream>
#include <vector>
#include "gtest/gtest.h"

/////////////////////////////////////////////////////////////////////////////
// PARTIE ENTETE

class Nombre {
private:
	struct Chiffre {
		unsigned int chiffre_;     // entre 0 et 9
		Chiffre * suivant_;        // chiffre de poids supérieur ou nullptr

		Chiffre(const Chiffre& c);
		Chiffre(unsigned long n);
		virtual ~Chiffre();
		void print(std::ostream & out);
	};
	Chiffre * premier_;

public:
	Nombre();
	Nombre(const Nombre& n);
	Nombre(unsigned long n);
	virtual ~Nombre();

	friend std::ostream & operator <<(std::ostream & out, const Nombre & n);
	friend std::istream & operator >>(std::istream & cin, Nombre & n);
	Nombre operator=(const Nombre & n);
	Nombre & operator+=(const unsigned int i);
	Nombre & operator+=(const Nombre& n2);
	Nombre & operator*=(const unsigned int i);
	Nombre & operator*=(const Nombre& n2);
	friend Nombre operator+(const Nombre& n, const unsigned int i);
  friend Nombre operator*(const Nombre& n, const unsigned int i);
  friend Nombre operator*(const Nombre& n, const Nombre& n2);
	void push_front(unsigned char n, Chiffre * ptr);
};

//////////////////////////////////////////////////////////////////////////////////
// PARTIE IMPLEMENTATION

///////// CONSTRUCTEURS /////////
Nombre::Nombre()
{
	premier_ = new Chiffre(0);
}

Nombre::Nombre(const Nombre& n)
{
	premier_ = new Chiffre(*(n.premier_));
}

Nombre::Nombre(unsigned long n)
{
	premier_ = new Chiffre(n);
}

Nombre::Chiffre::Chiffre(unsigned long n)
{
	chiffre_ = n % 10;
	if (n <= 9)
		suivant_ = NULL;
	else
		suivant_ = new Chiffre(n / 10);
}

Nombre::Chiffre::Chiffre(const Chiffre& c)
{
	chiffre_ = c.chiffre_;
	if (c.suivant_)
		suivant_ = new Chiffre(*(c.suivant_));
	else
		suivant_ = NULL;
}



///////// DESTRUCTEURS /////////

Nombre::~Nombre()
{
	if (premier_)
		delete premier_;
}
Nombre::Chiffre::~Chiffre()
{
	if (suivant_)
		delete suivant_;
}


///////// OPERATEURS /////////

std::ostream & operator <<(std::ostream & out, const Nombre & n)
{
	if (n.premier_ != NULL)
		n.premier_->print(out);
	return out;
}

std::istream & operator >>(std::istream & cin, Nombre & n)
{
	while (cin.good()) {
		int c{ cin.get() };
		if (std::isdigit(c)) {
			unsigned int d{ static_cast<unsigned int>(c - '0') };
			// d contient le chiffre entre 0 et 9 qui vient d'être lu
			n.push_front(d, n.premier_);
		}
		else break;
	}
	return cin;
}

Nombre Nombre::operator=(const Nombre &n)
{
	return n;
}

Nombre & Nombre::operator+=(const unsigned int i)
{
	*this += Nombre(i);
	return *this;
}

Nombre & Nombre::operator+=(const Nombre& n2)
{
	Chiffre * t = n2.premier_;
	Chiffre * n = this->premier_;
	unsigned char retenue = 0;		// retenue <= 1
	while (t || n)
	{
		if (!t->suivant_ && n->suivant_)
			t->suivant_ = new Chiffre(0);
		else if (t->suivant_ && !n->suivant_)
			n->suivant_ = new Chiffre(0);

		unsigned char res = t->chiffre_ + n->chiffre_ + retenue;
		if (res >= 10)
		{
			n->chiffre_ = res - 10;
			retenue = 1;
		}
		else
		{
			n->chiffre_ = res;
			retenue = 0;
		}

		if (!t->suivant_ && !n->suivant_ && retenue == 1)
		{
			n->suivant_ = new Chiffre(1);
			break;
		}
		t = t->suivant_;
		n = n->suivant_;
	}
	return *this;
}

Nombre operator+(const Nombre& n, const unsigned int i)
{
	Nombre res(n);
	res += i;
	return res;
}

Nombre & Nombre::operator*=(const unsigned int i)
{
	*this *= Nombre(i);
	return *this;
}

Nombre & Nombre::operator*=(const Nombre& n2)
{
	Chiffre * t = n2.premier_;		// operande 2
	std::vector<Nombre> toAdd;
	unsigned char dixaine = 0;
	
	while (t)
	{
		unsigned char retenue = 0;	// retenue <= 8 (9*9)
		Nombre add;
		Chiffre * a = add.premier_;
		for (unsigned int i = 0; i < dixaine; i++)
		{
			a->suivant_ = new Chiffre(0);
			a = a->suivant_;
		}
		Chiffre * n = this->premier_;	// operande 1
		while (n)
		{
			unsigned int res = n->chiffre_ * t->chiffre_ + retenue;

			if (res >= 10)
				retenue = res / 10;
			else
				retenue = 0;

			a->chiffre_ = res % 10;
			if(n->suivant_ || retenue != 0)
				a->suivant_ = new Chiffre(0);

			n = n->suivant_;
			a = a->suivant_;
		}

		if (retenue != 0)
			a->chiffre_ = retenue;

		toAdd.push_back(add);
		t = t->suivant_;
		dixaine++;
	}
	Nombre addition;
	for (unsigned int i = 0; i < toAdd.size(); i++)
	{
		addition += toAdd[i];
	}

	Chiffre * n = this->premier_;
	Chiffre * a = addition.premier_;
	while (a)
	{
		if (!n->suivant_ && a->suivant_)
			n->suivant_ = new Chiffre(0);
		n->chiffre_ = a->chiffre_;
		n = n->suivant_;
		a = a->suivant_;
	}

	return *this;
}

Nombre operator*(const Nombre& n, const unsigned int i)
{
	Nombre res(n);
	res *= i;
	return res;
}

Nombre operator*(const Nombre& n, const Nombre& n2)
{
	Nombre res(n);
	res *= n2;
	return res;
}


///////// METHODES /////////


void Nombre::Chiffre::print(std::ostream & out)
{
	if (suivant_ != NULL)
		suivant_->print(out);
	out << chiffre_;
}

void Nombre::push_front(unsigned char n, Chiffre * ptr)
{
	if (ptr->chiffre_ == 0 && ptr->suivant_ == NULL)
		ptr->chiffre_ = n;
	else if (ptr->suivant_ == NULL)
	{
		ptr->suivant_ = new Chiffre(ptr->chiffre_);
		ptr->chiffre_ = n;
	}
	else if (ptr->suivant_ != NULL)
	{
		push_front(ptr->chiffre_, ptr->suivant_);
		ptr->chiffre_ = n;
	}
}

///////////////////////////////////////////////////////////////////////////////
// PARTIE TEST
Nombre factorielle(const unsigned int n)
{
	if (n == 0)
		return Nombre(1);
	else
		return Nombre(n) * factorielle(n - 1);
}

TEST(TestNombre, TestNombreVide)
{
	Nombre n;
	std::ostringstream os;
	os << n;
	EXPECT_EQ(os.str(), "0");
}

TEST(TestNombre, TestNombreNul)
{
	Nombre n{ 0 };
	std::ostringstream os;
	os << n;
	EXPECT_EQ(os.str(), "0");
}

TEST(TestNombre, TestNombre12345678)
{
	Nombre n{ 12345678 };
	std::ostringstream os;
	os << n;
	EXPECT_EQ(os.str(), "12345678");
}

TEST(TestNombre, TestNombreCopie)
{
  Nombre n(12345678);
  Nombre n2(n);
  std::ostringstream os;
  os << n2;
  EXPECT_EQ(os.str(), "12345678");
}

TEST(TestNombre, TestNombreAffectation)
{
  Nombre n(12345678);
  Nombre n2 = n;
  std::ostringstream os;
  os << n2;
  EXPECT_EQ(os.str(), "12345678");
}

TEST(TestNombre, TestLectureGrandNombre)
{
    std::string big{ "123456789123456789123456789123456789" };
    std::istringstream ciin{ big };
    Nombre n;
    ciin >> n;
    std::ostringstream os;
    os << n;
    EXPECT_EQ( os.str(), big );
}

TEST(TestNombre, TestOperateursAddtitionRaccourci)
{
  std::string s = "0_198_252_1955";
  Nombre n1(0);   // Test 0 + 0
  Nombre n2(99);  // Test 99 + 99 (ajout de chiffre)
  Nombre n3(156); // Test 156 + 96 (2e nombre avec un chiffre en moins)
  Nombre n4(423); // Test 423 + 1532 (2e nombre avec un chiffre en plus)
  n1 += 0;
  n2 += 99;
  n3 += 96;
  n4 += 1532;
  std::ostringstream os;
  os << n1 << "_" << n2 << "_" << n3 << "_" << n4;
  EXPECT_EQ(os.str(), s);
}

TEST(TestNombre, TestOperateursAddtition)
{
  std::string s = "0_198_252_1955";
  Nombre n1(0);   // Test 0 + 0
  Nombre n2(99);  // Test 99 + 99 (ajout de chiffre)
  Nombre n3(156); // Test 156 + 96 (2e nombre avec un chiffre en moins)
  Nombre n4(423); // Test 423 + 1532 (2e nombre avec un chiffre en plus)
  Nombre n5 = n1 + 0;
  Nombre n6 = n2 + 99;
  Nombre n7 = n3 + 96;
  Nombre n8 = n4 + 1532;
  std::ostringstream os;
  os << n5 << "_" << n6 << "_" << n7 << "_" << n8;
  EXPECT_EQ(os.str(), s);
}

TEST(TestNombre, TestOperateursMultiplicationRaccourci)
{
  std::string s = "0_9801_14976_648036";
  Nombre n1(0);   // Test 0 * 0
  Nombre n2(99);  // Test 99 * 99 (ajout de chiffre)
  Nombre n3(156); // Test 156 * 96 (2e nombre avec un chiffre en moins)
  Nombre n4(423); // Test 423 * 1532 (2e nombre avec un chiffre en plus)
  n1 *= 0;
  n2 *= 99;
  n3 *= 96;
  n4 *= 1532;
  std::ostringstream os;
  os << n1 << "_" << n2 << "_" << n3 << "_" << n4;
  EXPECT_EQ(os.str(), s);
}

TEST(TestNombre, TestOperateursMultiplication)
{
  std::string s = "0_9801_14976_648036";
  Nombre n1(0);   // Test 0 * 0
  Nombre n2(99);  // Test 99 * 99 (ajout de chiffre)
  Nombre n3(156); // Test 156 * 96 (2e nombre avec un chiffre en moins)
  Nombre n4(423); // Test 423 * 1532 (2e nombre avec un chiffre en plus)
  Nombre n5 = n1 * 0;
  Nombre n6 = n2 * 99;
  Nombre n7 = n3 * 96;
  Nombre n8 = n4 * 1532;
  std::ostringstream os;
  os << n5 << "_" << n6 << "_" << n7 << "_" << n8;
  EXPECT_EQ(os.str(), s);
}

TEST( TestNombre, TestFactorielle123 )
{
    std::ostringstream os;
    os << factorielle( 123 );;
    EXPECT_EQ( os.str(), "12146304367025329675766243241881295855454217088483382315328918161829235892362167668831156960612640202170735835221294047782591091570411651472186029519906261646730733907419814952960000000000000000000000000000" );
}