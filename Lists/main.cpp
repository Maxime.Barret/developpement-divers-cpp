#include <iostream>
#include <vector>
#include <forward_list>
#include <cstdlib>
#include <ctime>
#include <string>
#include <functional>
#include <algorithm>
#include <limits>

#define RECUR

typedef std::forward_list<int> FLIST;	// alias pour facilite l'ecriture

void print_tab(const std::vector<int> &tab, const std::string str = "");
void print_list(const FLIST &list, const std::string str = "");
void random_tab(std::vector<int> &tab);
FLIST random_list(const int size);

void sort_tab_1(std::vector<int> &tab);
void sort_tab_2(std::vector<int> &tab, std::function<bool(const int, const int)> comp);

FLIST map(const FLIST &list, std::function<int(int)> func);
FLIST filter(const FLIST &list, std::function<bool(int)> func);
int reduce(const FLIST &list, const int n, std::function<int(int, int)> func);

int fold_left_aux(FLIST::const_iterator current, FLIST::const_iterator lend, const int n, std::function<int(int, int)> func);
int fold_left(const FLIST &list, const int n, std::function<int(int, int)> func);
FLIST map_aux(FLIST::const_iterator current, FLIST::const_iterator lend, std::function<int(int)> func);
FLIST filter_aux(FLIST::const_iterator current, FLIST::const_iterator lend, std::function<bool(int)> func);

bool less(const int n1, const int n2);
bool greater(const int n1, const int n2);

void test_1();
void test_2();
void test_3();
void test_4();

int main()
{
	std::srand(std::time(0));
	test_1();
	test_2();
	test_3();
	test_4();
}


// Fonction de test
//
void test_1()
{
	std::vector<int> tab(10);
	std::cout << "test 1 : " << std::endl;

	// Randomisation du vecteur 
	random_tab(tab);
	print_tab(tab, "Tableau random :");

	// Tri du vecteur
	sort_tab_1(tab);
	print_tab(tab, "Tableau trie :");
}


// Fonction de test 2
//
void test_2()
{
	std::vector<int> tab(10);
	std::cout << "\ntest 2 : " << std::endl;

	// Randomisation du vecteur 
	random_tab(tab);
	print_tab(tab, "Tableau random :");

	// Tri du vecteur du + grand au + petit
	sort_tab_2(tab, [](int n1, int n2) {return n1 < n2; });
	print_tab(tab, "Tableau trie - -> + :");

	// Tri du vecteur du + petit au + grand
	sort_tab_2(tab, [](int n1, int n2) {return n1 > n2; });
	print_tab(tab, "Tableau trie + -> - :");
}


// Fonction de test 3
//
void test_3()
{
	std::cout << "\ntest 3 : " << std::endl;
	int coef = 1 + rand() % 5;
	auto mult_coef = std::bind(std::multiplies<int>(), coef, std::placeholders::_1);	// curryfication de la fonction multiplies()
	auto is_pair = std::bind(std::equal_to<int>(), std::bind(std::modulus<int>(), std::placeholders::_1, 2), 0);		// curryfication de la verification de parite d'un entier

	// Initialisation de la liste
	const FLIST list = random_list(10);
	print_list(list, "Liste random :");

	// Mapping de la liste
	const FLIST list_map = map(list, [mult_coef](int n) {return mult_coef(n); });
	std::cout << "coef = " << coef << std::endl;
	print_list(list_map, "Liste mappee :");

	// Suppression des elements impairs
	const FLIST list_pair = filter(list, [is_pair](int n) {return is_pair(n); });
	print_list(list_pair, "Liste des elements pairs :");
}


// Fonction de test 4
//
void test_4()
{
	int petit;
	int grand;
	std::cout << "\ntest 4 : " << std::endl;

	// Initialisation de la liste
	const FLIST list = random_list(10);
	print_list(list, "Liste random :");

	// reduce pour le + petit et le + grand
	//petit = reduce(list, INT_MAX, [&grand](int a, int b) { grand = grand < b ? b : grand; return a < b ? a : b; });

	// reduce de maniere recursive
	petit = fold_left(list, INT_MAX, [&grand](int a, int b) { grand = grand < b ? b : grand; return a < b ? a : b; });
	std::cout << "Le plus petit est : " << petit << std::endl;
	std::cout << "Le plus grand est : " << grand << std::endl;
}


// Fonction d'affichage d'un vecteur
//
void print_tab(const std::vector<int> &tab, const std::string str)
{
	std::cout << str << std::endl;
	std::cout << "(";
	for (int i = 0; i < tab.size(); i++)
		std::cout << " " << tab[i];
	std::cout << " )" << std::endl;
}


// Fonction d'affichage d'une forward_list
//
void print_list(const FLIST &list, const std::string str)
{
	std::cout << str << std::endl;
	std::cout << "(";
	for(int i : list)
		std::cout << " " << i;
	std::cout << " )" << std::endl;
}


// Fonction de randomisation d'un tableau
//
void random_tab(std::vector<int> &tab)
{
	for (int i = 0; i < tab.size(); i++)
		tab[i] = rand() % 100;
}


// Fonction de randomisation d'une forward_list
FLIST random_list(const int size)
{
	if (size < 0)
	{
		std::cout << "------- ERR size < 0 ..." << std::endl;
		exit(EXIT_FAILURE);
	}	
	else
	{
		FLIST list;
		for (int i = 0; i < size; i++)
			list.push_front(rand() % 100);
		return list;
	}
}


// Fonction de tri
//
void sort_tab_1(std::vector<int> &tab)
{
	// Tri par insertion
	int n = tab.size();
	for (int i = 1; i < n; i++)
	{
		int x = tab[i];
		int j = i;
		while (j > 0 && tab[j - 1] > x)
		{
			tab[j] = tab[j - 1];
			j--;
		}
		tab[j] = x;
	}
}

// Fonction de tri par pointeur de fonction
//
void sort_tab_2(std::vector<int> &tab, std::function<bool(const int, const int)> comp)
{
	std::sort(tab.begin(), tab.end(), comp);
}


// Fonction appliquant func aux elements d'une forward_list
//
FLIST map(const FLIST &list, std::function<int(int)> func)
{
#ifndef RECUR
	FLIST res(list);
	auto resit = res.begin();								// utilisation d'iterateurs pour avoir la liste resultats dans le meme ordre que la liste argument
	for (auto it = list.begin(); it != list.end(); it++)
	{
		*resit = func(*it);
		resit++;
	}
		
	return res;
	/* Reponse a la question de l'ordre des elements : 
	
	Avant d'appliquer les correction d'ordre, j'avais ecrit ceci :

	-------------------------------------------------------------
	std::forward_list<int> res;
	for (int i : liste)
		res.push_front(func(i));

	return res;
	-------------------------------------------------------------

	Ce code pousse toute valeur de res pour ajouter les nouvelles valeurs devant les autres elements.
	Or, nous parcourons la liste argument dans l'ordre 0 -> ++, ce qui fais que la liste resulat sera dans l'ordre ++ -> 0

	*/
#else
	return map_aux(list.cbegin(), list.cend(), func);
#endif
}


// Fonction de filtrage d'une liste selon yun predicat
//
FLIST filter(const FLIST &list, std::function<bool(int)> func)
{
#ifndef RECUR
	FLIST res;
	for (int i : list)
	{
		if (func(i))
			res.push_front(i);
	}
	return res;
#else
	return filter_aux(list.cbegin(), list.cend(), func);
#endif
}


// Fonction reduce
//
int reduce(const FLIST &list, const int n, std::function<int(int, int)> func)
{
	bool first = true;
	int res;
	for (int i : list)
	{
		if (first)
		{
			res = func(n, i);
			first = false;
		}
		else
			res = func(res, i);
	}
	return res;
}


// Fonction reduce de maniere recursive
//
int fold_left_aux(FLIST::const_iterator current, FLIST::const_iterator lend, const int n, std::function<int(int, int)> func)
{
	if (current == lend)
		return n;
	else
		return func(fold_left_aux(++current, lend, n, func), *current);
}


// Fonction d'appel de la recursivite
//
int fold_left(const FLIST &list, const int n, std::function<int(int, int)> func)
{
	return fold_left_aux(list.cbegin(), list.cend(), n, func);
}


// Fonction auxiliere de mapping
//
FLIST map_aux(FLIST::const_iterator current, FLIST::const_iterator lend, std::function<int(int)> func)
{
	if (current == lend)
		return FLIST(current, lend);
	else
	{
		int bckup = func(*current);
		FLIST interm = map_aux(++current, lend, func);
		interm.push_front(bckup);
		return interm;
	}
}


// Fonction auxiliere de filtrage par predicat
//
FLIST filter_aux(FLIST::const_iterator current, FLIST::const_iterator lend, std::function<bool(int)> func)
{
	if (current == lend)
		return FLIST(current, lend);
	else
	{
		int bckup = *current;
		FLIST interm = filter_aux(++current, lend, func);
		if(func(bckup))
			interm.push_front(bckup);
		return interm;
	}
}


// Renvoie true si n1 < n2
//
bool less(const int n1, const int n2)
{
	return n1 < n2;
}


// Renvoie true si n2 < n1
//
bool greater(const int n1, const int n2)
{
	return n1 > n2;
}